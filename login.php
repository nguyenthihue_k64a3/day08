<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />
    <link rel="stylesheet" href="login.css"> 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <title>Document</title>
</head>
<body>
<?php

session_start();
$gtinh = array("0" => "Nam", "1" => "Nữ");
$khoa = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
$nameErr =$genderErr = $khoaErr = $dateErr = $fileErr =  "";
$numErr = 0;
$target_dir = "./uploads/";


$allowedTypes = [
    'image/png' => 'png',
    'image/jpeg' => 'jpg'
];
//xác thực form bằng php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["name"])) {
        $nameErr = "Hãy nhập tên";
        $numErr ++;
    }

    if (empty($_POST["gtinh"])) {
        $genderErr = "Hãy chọn giới tính";
        $numErr ++;
    }

    if (empty($_POST["khoa"])) {
        $khoaErr ="Hãy chọn phân khoa";
        $numErr ++;
    }

    if (empty($_POST["birthday"])) {
        $dateErr = "Hãy nhập ngày sinh";
        $numErr ++;
    }else if (!validateBirthday($_POST["birthday"])){
        $dateErr = "Hãy nhập ngày sinh đúng định dạng";
        $numErr++;
    }

    $filepath = $_FILES['file']['tmp_name'];
    $fileinfo = finfo_open(FILEINFO_MIME_TYPE);
    $filetype = finfo_file($fileinfo, $filepath);

    if(!in_array($filetype, array_keys($allowedTypes))) {
        $fileErr = "Hãy chọn lại file. File isn't imgage";
        $numErr ++;

    }

    if ($numErr == 0) {
        mkdir($target_dir, "0700");
        
        $date = date('YmdHis');
        $fomat = explode('.', $_FILES["file"]["name"]);
        $filename = $fomat[0].'_'.$date.'.'.$fomat[1];
        $target_file = $target_dir . basename($filename);

        move_uploaded_file($filepath, $target_file);
        $_POST["file"] = $target_file;
        $_SESSION = $_POST;
        header("Location: confirm.php");
    }
    
}


function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

function validateBirthday($birthday){
    $birthdays  = explode('/', $birthday);
    if (count($birthdays) == 3) {
      return checkdate($birthdays[1], $birthdays[0], $birthdays[2]);
    }
    return false;
}

//


?>
    <form method="post" enctype="multipart/form-data" action="<?php 
         echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <p class="error"><?php
              if ($nameErr != "") echo $nameErr."<br>";
              if ($genderErr != "") echo $genderErr."<br>";
              if ($khoaErr != "") echo $khoaErr."<br>";
              if ($dateErr != "") echo $dateErr."<br>";
              if ($fileErr != "") echo $fileErr."<br>";
              
        ?></p>
        <div class="div">
            <label class="label-left">Họ và tên <span class="error">* </span></label>
            <input class="fullname" name="name" type="text" id="fullname"/></div>
        </div>
        <div class="div">
            <label class="label-left">Giới tính <span class="error">* </span></label>
            <?php
            for ($i = 0; $i < count($gtinh); $i++) {
                echo "<input type=\"radio\" class=\"ip radio\" name=\"gtinh\" value=\"$gtinh[$i]\">
                    <label for=\"{$i}\" class=\"gtinh\">{$gtinh[$i]}</label>\n";

            };
            ?>
        </div>
        <div class="div">
            <label class="label-left">Phân Khoa <span class="error">* </span></label>
                <select name="khoa" id="khoa" class="khoa">
                    <option value=""></option>
                    <?php
                    foreach ($khoa as $key => $value) {
                        echo "\t<option value=\"{$value}\">{$value}</option>\n";
                    };
                    ?>
                </select>
        </div>
        <div class="date">
            <label class="label-left" for="date">Ngày sinh <span class="error">* </span></label>
            <div class="input-group">
                <input class="form__input" id="birthday" name="birthday" placeholder="dd/mm/yyyy" type="text"/>
            </div>
        </div>
        <div class="div">
            <label class="label-left">Địa chỉ</label>
            <input type="text" class="address" name="dchi" id="address"/>
        </div>
        <div class="div">
            <label class="label-left">Hình ảnh</label>
            <input type="file" name="file" id="file" accept=".png, .jpg, .jpeg"/>
        </div>
        <button value="Upload Image" name="submit">Đăng nhập</button>,
    </form>
    <script>
            $(document).ready(function(){
                var date_input=$('input[name="birthday"]');
                var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                date_input.datepicker({
                    format: 'dd/mm/yyyy',
                    container: container,
                    todayHighlight: true,
                    autoclose: true,
                })
            })
    </script>

</body>
    
</html>