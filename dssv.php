<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="dssv.css">
    <title>DSSV</title>
    
</head>


<body>
<?php
    $is_page_refreshed = (isset($_SERVER['HTTP_CACHE_CONTROL']) && $_SERVER['HTTP_CACHE_CONTROL'] == 'max-age=0');
    
    if (isset($_POST['select_khoa'])) {
        $_SESSION['select_khoa'] = $_POST['select_khoa'];
    }
    if (isset($_POST['key'])){
        $_SESSION['key'] = $_POST['key'];
    }
?>
<form method="post" enctype="multipart/form-data" action="<?php 
         echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"> 
<div class="header">
    <div class="header1">
        <div class="item">
        <div class="item_left">Khoa</div>

<div class="item_right" >
    <select class="drop" name ='select_khoa' id="select_khoa">
        <?php
            $phankhoa = array(
                "MTA" => "Khoa học máy tính",
                "KDL" => "Khoa học dữ liệu"

            );
           
            echo "<option value=''</option>";
            foreach($phankhoa as $class => $value) {
                echo "<option value= '". $class . "'";
                if(array_key_exists('search',$_POST)) {
                    if ($is_page_refreshed){
                        if ($_SESSION['select_khoa'] === $class) {
                            echo "selected";   
                        }
                    }
                }    
                     
                echo ">$value</option>";
            }
              
        ?>   
    </select>
</div>

</div>

<div class="item">

<div class="item_left" >Từ khóa</div>

<div class="item_right">
    <input type="text" class="drop key" name ="key" id ="text" 
    <?php
        if(array_key_exists("search",$_POST)) {
            if ($is_page_refreshed){
                echo "value='" . $_SESSION["key"] ."'";
            }
        }
    ?>
    >
    
</div>

</div>
<div class="item item_seach">
<button id="button" class="search delete">Xóa</button>
<button name="search"id ="search" class="search">Tìm kiếm</button>

</div>



<!-- clear các giá trị khoa và khóa -->


</div>
<div class = 'row'>
    <div class="item left show_sv">Số sinh viên tìm thấy:</div>
    <div class="login"><a href="login.php" class="add">Thêm</a></div>  
</div>


    <table style="width:90%">
        <tr>
            <th>No</th>
            <th>Tên sinh viên</th>
            <th>Khoa</th>
            <th>Action</th>
        </tr>
        <tr>
            <td>1</td>
            <td>Nguyen Van A</td>
            <td>Khoa học máy tính</td>
            <td>
                <div class="item_right_con">
                    <button class="delete">Xóa</button>
                    <button class="delete">Sửa</button>
                </div>
            </td>
        </tr>
        <tr>
            <td>1</td>
            <td>Nguyen Van A</td>
            <td>Khoa học máy tính</td>
            <td>
                <div class="item_right_con">
                    <button class="delete">Xóa</button>
                    <button class="delete">Sửa</button>
                </div>
            </td>
        </tr>

        <tr>
            <td>2</td>
            <td>Nguyen Van B</td>
            <td>Khoa học máy tính</td>
            <td>
                <div class="item_right_con">
                    <button class="delete">Xóa</button>
                    <button class="delete">Sửa</button>
                </div>
            </td>
        </tr>

        <tr>
            <td>3</td>
            <td>Nguyen Van C</td>
            <td>Khoa học máy tính</td>
            <td>
                <div class="item_right_con">
                    <button class="delete">Xóa</button>
                    <button class="delete">Sửa</button>
                </div>
            </td>
        </tr>

        <tr>
            <td>4</td>
            <td>Nguyen Van D</td>
            <td>Khoa học vật liệu</td>
            <td>
                <div class="item_right_con">
                    <button class="delete">Xóa</button>
                    <button class="delete">Sửa</button>
                </div>
            </td>
        </tr>
</table>

</div>
</form>

    
</body>
</html>